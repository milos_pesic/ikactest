package test.assignment.data.source.remote;

import java.util.List;

import rx.Observable;
import test.assignment.data.BreedModel;

/**
 * Created by milospesic on 8/5/17.
 */

public interface RemoteDogDataSource {
    Observable<List<BreedModel>> getAllBreeds();
    Observable<String> getSubBreedImage(String breed, String subbreed);
}
