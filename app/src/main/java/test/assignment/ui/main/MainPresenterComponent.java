package test.assignment.ui.main;

import dagger.Component;
import test.assignment.commons.ioc.scopes.ActivityScope;
import test.assignment.data.source.DogRepositoryComponent;

/**
 * Created by milospesic on 8/5/17.
 */

@ActivityScope
@Component(dependencies = {DogRepositoryComponent.class},  modules = { MainPresenterModule.class} )
public interface MainPresenterComponent {

    void inject(MainActivity activity);
}
