package test.assignment.commons.ui;

/**
 * Created by milospesic on 8/5/17.
 */

public enum ListType {
    VERTICAL_LIST,
    GRID
}
